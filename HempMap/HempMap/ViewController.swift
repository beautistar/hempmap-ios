//
//  ViewController.swift
//  HempMap
//
//  Created by Yin on 2018/10/10.
//  Copyright © 2018 Yin. All rights reserved.
//

import UIKit

class ViewController: UIViewController {

    @IBOutlet weak var webView: UIWebView!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        // Do any additional setup after loading the view, typically from a nib.
        
        let url = URL(string: "http://hempmaps.us")
        
        if let unwarppedURL = url {
            let request = URLRequest(url: unwarppedURL)
            let session = URLSession.shared
            
            let task = session.dataTask(with: request) {(data, response, error) in
                
                if error == nil {
                    self.webView.loadRequest(request)
                } else {
                    print("ERROR: \(String(describing: error))")
                }
            }
            
            task.resume()
        }
    }


}

